/*Con el metodo de pasar un segundo parametro true para operadores y false para operandos, se hace mas facil luego añadir botones */
function printDigit(value){

    var screen = document.getElementById("screen");
    var upperScreen = document.getElementById("upperScreen");

    if (value=="0" && (screen.innerHTML.charAt(0)!="0" || screen.innerHTML.includes('.'))){
            screen.innerHTML += value;
    }

    else if (value == "."){

        if(screen.innerHTML!="" && !screen.innerHTML.includes('.')){
            screen.innerHTML += value;
        }
    }

    else if (value=="+" || value=="-" || value=="*" || value=="/"){

        if (screen.innerHTML!="" && upperScreen.innerHTML!=""){
            calculate();
            upperScreen.innerHTML = upperScreen.innerHTML + " " + value;
        }
        
        else if(upperScreen!="" && screen.innerHTML=="" && !(upperScreen.innerHTML.includes('+') || (upperScreen.innerHTML.includes('-') && upperScreen.innerHTML.charAt(0)!="-"))  || upperScreen.innerHTML.includes('*') || upperScreen.innerHTML.includes('/')){
            upperScreen.innerHTML +=" " + value;
        }

        else if (screen.innerHTML!="" && !(screen.innerHTML.includes('+') || screen.innerHTML.includes('-') || screen.innerHTML.includes('*') || screen.innerHTML.includes('/'))){
            upperScreen.innerHTML = screen.innerHTML + " " + value;
            screen.innerHTML = "";
        }
    }

    else{
        if (screen.innerHTML.length==1 && screen.innerHTML.charAt(0)=="0"){
            screen.innerHTML = value;
        }
        else {
            screen.innerHTML += value;
        }
    }
}

function deleteDigit(value){

    var screen = document.getElementById("screen");

    if (value == "last" && screen.innerHTML.length!=0){
        screen.innerHTML = screen.innerHTML.substring(0, screen.innerHTML.length-1);
    }

    else if (value == "ce" && screen.innerHTML!="" && screen.innerHTML!="0"){
        screen.innerHTML = "";
    }

    else if (value == 'c'){
        screen.innerHTML = "";
        document.getElementById("upperScreen").innerHTML="";
    }

    if (screen.innerHTML.length == 0){
        screen.innerHTML = "0";
    }
}

function calculate(){
    var screen = document.getElementById("screen");
    var upperScreen = document.getElementById("upperScreen");
    
    if (screen.innerHTML!="" && upperScreen.innerHTML!=""){
        document.getElementById("upperScreen").innerHTML = eval(document.getElementById("upperScreen").innerHTML + document.getElementById("screen").innerHTML);
        document.getElementById("screen").innerHTML = "";
    }
}